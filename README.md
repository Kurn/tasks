
package task1;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        task_1();
    }
    
    public static void task_1() {
        Scanner in = new Scanner(System.in);
        System.out.print("Enter the symbol: ");
        String s = in.nextLine();
        System.out.printf("%100s%n", s); 
        System.out.printf("%99s%s%s%n", s, s, s);
        System.out.printf("%98s%s%s%s%s%n", s, s, s, s, s);
        System.out.printf("%97s%s%s%s%s%s%s%n", s, s, s, s, s, s, s);
        System.out.printf("%97s%s%s%s%s%s%s%n", s, s, s, s, s, s, s);
        System.out.printf("%97s%s%s%s%s%s%s%n", s, s, s, s, s, s, s);
        System.out.printf("%98s%s%s%s%s%n", s, s, s, s, s);
        System.out.printf("%99s%s%s%n", s, s, s);
        System.out.printf("%100s%n", s);   
    }
    
    public static void task_2() {
        Scanner in = new Scanner(System.in);
        System.out.print("Enter the number x: ");
        double x = in.nextDouble();
        System.out.print("Enter the number a: ");
        double a = in.nextDouble();
        System.out.print("Enter the number k: ");
        double k = in.nextDouble();
        double t, u;
        if (2 + x <= 0) System.out.println("x must be greater than -2");
        else {
            t = 2 * k / a - Math.log(2 + x) + x * x;
            if (t + 1 < 0) System.out.println("x^2-ln(2+x)+2*k/a+1 must be greater than or equal to 0");
            else {
                u = Math.sqrt(t + 1) / (t * t - t + 1);
                System.out.println("t=" + t);
                System.out.println("u=" + u);
            }
        }
    }
    
    public static void task_3() {
        Scanner in = new Scanner(System.in);
        System.out.print("Enter the number A: ");
        int a = in.nextInt();
        System.out.print("Enter the number B: ");
        int b = in.nextInt();
        
        System.out.println("A=" + Integer.toBinaryString(a));
        System.out.println("B=" + Integer.toBinaryString(b));
        a = a << 1;
        a = a & 248;
        int c = b << 5;
        c = c | b;
        c = ~248 & c;
        c = c | a;
        
        System.out.println("result=" + c);
        System.out.println("binary result=" + Integer.toBinaryString(c));
    }
    
    public static void task_4() {
        Scanner in = new Scanner(System.in);
        System.out.print("Enter the number: ");
        int n = in.nextInt();
        if (n < 0) n = Math.abs(n);
        int len = Integer.toString(n).length();
        int prod = 1;
        for (int i = 0; i < len; i++) {
            prod *= n % 10;
            n = (n - n % 10) / 10;
        }
        System.out.println(prod);
    }
    
    public static void task_5() {
        Scanner in = new Scanner(System.in);
        System.out.print("Enter the number a: ");
        int a = in.nextInt();
        System.out.print("Enter the number b: ");
        int b = in.nextInt();
        System.out.print("Enter the number c: ");
        int c = in.nextInt();
        System.out.print("Enter the number r: ");
        int r = in.nextInt();
        System.out.print("Enter the number s: ");
        int s = in.nextInt();
        if (a <= 0 || b <= 0 || c <= 0 || r <= 0 || s <= 0) System.out.println("All variables must be greater than 0");
        else {
            if (Math.min(Math.min(a, b), c) > Math.min(r, s)) System.out.println(false);
            else {
                if (Math.min(Math.min(a, b), c) == a) {
                    if (Math.min(r, s) == r) {
                        if (Math.min(b, c) > s) System.out.println(false);
                        else System.out.println(true);
                    }
                    else {
                        if (Math.min(b, c) > r) System.out.println(false);
                        else System.out.println(true);
                    }
                }
                else if (Math.min(Math.min(a, b), c) == b) {
                    if (Math.min(r, s) == r) {
                        if (Math.min(a, c) > s) System.out.println(false);
                        else System.out.println(true);
                    }
                    else {
                        if (Math.min(a, c) > r) System.out.println(false);
                        else System.out.println(true);
                    }
                }
                else {
                    if (Math.min(r, s) == r) {
                        if (Math.min(a, b) > s) System.out.println(false);
                        else System.out.println(true);
                    }
                    else {
                        if (Math.min(a, b) > r) System.out.println(false);
                        else System.out.println(true);
                    }
                }
            } 
        }
    }
    
    public static void task_6() {
        Scanner in = new Scanner(System.in);
        System.out.print("Enter the number p: ");
        double p = in.nextDouble();
        System.out.print("Enter the number q: ");
        double q = in.nextDouble();
        try {
            if (p % q == 0) System.out.println(true);
            else System.out.println(false);
        }
        catch (Exception ex) {
            System.out.println(ex.getMessage() + " " + false);
        }
    }
    
    public static void task_7() {
        Scanner in = new Scanner(System.in);
        System.out.print("Enter the number : ");
        int n = in.nextInt();
        if (n < 0) n = Math.abs(n);
        int len = Integer.toString(n).length();
        if (n % 10 + ((n - n % 10) / 10) % 10 == (n - n % Math.pow(10,len - 1)) / Math.pow(10,len - 1))
            System.out.println(true);
        else 
            System.out.println(false);
    }
    
    public static void task_8() {
        Scanner in = new Scanner(System.in);
        System.out.print("Enter the number x: ");
        double x = in.nextDouble();
        System.out.print("Enter the number y: ");
        double y = in.nextDouble();
        System.out.print("Enter the number z: ");
        double z = in.nextDouble();
        
        double arr[] = new double[]{x, y, z};
        for (int i = 0; i < arr.length - 1; i++)
            for (int j = i + 1; j < arr.length; j++) 
                if (arr[i] < arr[j]) {
                    double temp = arr[i];
                    arr[i] = arr[j];
                    arr[j] = temp;
                }
        System.out.println("x=" + arr[0]);
        System.out.println("y=" + arr[1]);
        System.out.println("z=" + arr[2]);
    }
    
}
